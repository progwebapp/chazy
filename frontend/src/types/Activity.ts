export type Activity = {
  id: number
  name: string
  type: "game" | "movie" | "work"
}
