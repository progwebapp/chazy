import { defineStore } from "pinia";
import { Ref, ref } from "vue";

export const useActivityDialogStore = defineStore('activityDialog', () => {
  const dialog: Ref<boolean> = ref(true);

  const setDialog = (value: boolean) => {
    dialog.value = value
  }

  return {
    dialog,

    setDialog
  };
});
