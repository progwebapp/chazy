# Chazy : Database

![Icon](../icon.png)

## Table Of Contents

- [Chazy : Database](#chazy--database)
  - [Table Of Contents](#table-of-contents)
  - [Database](#database)
  - [Schema](#schema)

## Database

**Tables** :

- **users** : Users Management
- **activities** : Activities Management

**Enums** :

- **Activity Type** :
  - Work
  - Movie
  - Game

## Schema

![Database](./img/database.png)
